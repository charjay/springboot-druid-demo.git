package com.charjay.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.charjay.bean.User;

@Mapper
public interface UserMapper{

	List<User> list();

	User getOne(int id);
	
}
