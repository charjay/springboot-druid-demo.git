package com.charjay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ServletComponentScan//Servlet扫描！！！
@ImportResource(locations = { "classpath:druid-bean.xml" })
public class App {

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}
}
